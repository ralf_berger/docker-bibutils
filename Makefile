.PHONY: all build info

I=regreb/bibutils:latest
D=docker run -i $(I)

all: build info

build:
	@echo "Building $(I) ..."
	@docker build . --quiet -t $(I) > /dev/null

info:
	@echo "\nImage:"
	@docker images $(I)

	@echo "\nVersion:"
	@$(D) xml2bib -v

	@echo "\nBinaries:"
	@$(D) dpkg -L bibutils | grep '/usr/local/bin/' | xargs basename
