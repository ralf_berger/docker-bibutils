# Bibutils

This is two projects in one:

1. A Docker image containing the latest version of Bibutils.
2. Wrappers to run the Bibutils tools (using Docker), as if they were local executables.


## 1. Build the image

* `make`


## 2. Run the Bibutils tools

E.g. to convert a MODS file (existing outside the container) to BibTex:

```sh
cat bib.xml  | ./bin/xml2bib /dev/stdin
```
